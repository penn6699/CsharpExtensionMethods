﻿using System;


/// <summary>
/// Guid 助手
/// </summary>
public class GuidHelper
{
    #region static invoking

    /// <summary>
    /// Guid模式
    /// </summary>
    public enum GuidMode
    {
        /// <summary>
        /// e0a953c3ee6040eaa9fae2b667060e09
        /// </summary>
        N,
        /// <summary>
        /// 9af7f46a-ea52-4aa3-b8c3-9fd484c2af12
        /// </summary>
        D,
        /// <summary>
        /// {734fd453-a4f8-4c5d-9c98-3fe2d7079760}
        /// </summary>
        B,
        /// <summary>
        /// (ade24d16-db0f-40af-8794-1e08e2040df3)
        /// </summary>
        P,
        /// <summary>
        /// {0x3fa412e3,0x8356,0x428f,{0xaa,0x34,0xb7,0x40,0xda,0xaf,0x45,0x6f}}
        /// </summary>
        X
    }

    /// <summary>
    /// 生成的 Guid 格式：e0a953c3ee6040eaa9fae2b667060e09
    /// </summary>
    public static string N
    {
        get {
            return System.Guid.NewGuid().ToString("N");
        }
    }
    /// <summary>
    /// 生成的 Guid 格式：9af7f46a-ea52-4aa3-b8c3-9fd484c2af12
    /// </summary>
    public static string D
    {
        get
        {
            return System.Guid.NewGuid().ToString("D");
        }
    }
    /// <summary>
    /// 生成的 Guid 格式：{734fd453-a4f8-4c5d-9c98-3fe2d7079760}
    /// </summary>
    public static string B
    {
        get
        {
            return System.Guid.NewGuid().ToString("B");
        }
    }
    /// <summary>
    ///生成的 Guid 格式：(ade24d16-db0f-40af-8794-1e08e2040df3)
    /// </summary>
    public static string P
    {
        get
        {
            return System.Guid.NewGuid().ToString("P");
        }
    }
    /// <summary>
    ///生成的 Guid 格式：{0x3fa412e3,0x8356,0x428f,{0xaa,0x34,0xb7,0x40,0xda,0xaf,0x45,0x6f}}
    /// </summary>
    public static string X
    {
        get
        {
            return System.Guid.NewGuid().ToString("X");
        }
    }

    /// <summary>
    /// 将 GUID 的字符串表示形式转换为等效的 System.Guid 结构。
    /// </summary>
    /// <param name="guid">GUID 的字符串</param>
    /// <returns></returns>
    public static Guid Parse(string guid) {
        return Guid.Parse(guid);
    }

    /// <summary>
    /// 将 GUID 的字符串表示形式转换为等效的 System.Guid 结构，前提是该字符串采用的是指定格式。
    /// </summary>
    /// <param name="guid">GUID 的字符串表示形式</param>
    /// <param name="mode">GUID 模式</param>
    /// <returns></returns>
    public static Guid Parse(string guid,GuidMode mode)
    {
        return Guid.ParseExact(guid, Enum.GetName(typeof(GuidMode), mode));
    }

    #endregion

    private Guid _guid;
    public GuidHelper() {
        _guid = Guid.NewGuid();
    }
    public GuidHelper(string guid)
    {
        _guid = Guid.Parse(guid);
    }

    /// <summary>
    /// Guid 格式化
    /// </summary>
    /// <param name="mode">Guid 模式</param>
    /// <returns></returns>
    public string Format(GuidMode mode = GuidMode.D) {
        return _guid.ToString(Enum.GetName(typeof(GuidMode), mode));
    }

    /// <summary>
    /// 生成新的 Guid
    /// </summary>
    /// <param name="mode">Guid 模式</param>
    /// <returns></returns>
    public string NewGuid(GuidMode mode)
    {
        return Guid.NewGuid().ToString(Enum.GetName(typeof(GuidMode), mode));
    }

    /// <summary>
    /// 生成新的 Guid
    /// </summary>
    /// <returns></returns>
    public Guid NewGuid()
    {
        return Guid.NewGuid();
    }

}







