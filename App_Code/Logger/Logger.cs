﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using ApiLogger;

/*
 * 日志助手 v2.0
 * 具有调试信息记录、数据跟踪信息记录、错误信息记录、离线阅读日志等功能
 * Create by Penn.Lin 2018-10-08
 */

/// <summary>
/// Logger 的摘要说明
/// </summary>
public static class Logger
{

    #region Log Write

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Message"></param>
    /// <param name="Data"></param>
    /// <param name="LogType"></param>
    public static void Error(string Message, object Data = null, string LogType = "系统") {
        try
        {
            if (LogLevel.CanLog("error"))
            {
                string file = LogFormatter.Format("error", Message, Data, LogType);
                LogFile.Write(file);
            }
        }
        catch { }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Message"></param>
    /// <param name="Data"></param>
    /// <param name="LogType"></param>
    public static void Trace(string Message, object Data = null, string LogType = "系统")
    {
        try
        {
            if (LogLevel.CanLog("trace"))
            {
                string file = LogFormatter.Format("trace", Message, Data, LogType);
                LogFile.Write(file);
            }
        }
        catch { }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="Message"></param>
    /// <param name="Data"></param>
    /// <param name="LogType"></param>
    public static void Debug(string Message, object Data = null, string LogType = "系统")
    {
        try
        {
            if (LogLevel.CanLog("debug"))
            {
                string file = LogFormatter.Format("debug", Message, Data, LogType);
                LogFile.Write(file);
            }
        }
        catch { }
    }

    #endregion


    #region Log Read

    /// <summary>
    /// 读取日志文件
    /// </summary>
    /// <param name="fileName"></param>
    /// <returns></returns>
    public static List<Dictionary<string, string>> ReadLogFile(string fileName = null) {
        List<Dictionary<string, string>> list = new List<Dictionary<string, string>>();

        string file = LogFile.ReadLogFile(fileName) ?? "";
        string[] table = file.Split(new string[] { "\r\n" }, StringSplitOptions.None);     
        foreach (string row in table)
        {
            Dictionary<string, string> dic = new Dictionary<string, string>();
            string[] cols = row.Split('\t');
            if (cols.Length > 1)
            {
                ////时间 级别 类型 消息 数据 使用用户ID 使用用户Name
                list.Add(
                    new Dictionary<string, string>
                    {
                        { "Time",cols[0]}
                        ,{ "LogLevel",cols[1]}
                        ,{ "Type",cols[2]}
                        ,{ "Message",cols[3]}
                        ,{ "Data",cols[4]}
                        ,{ "UserID",cols[5]}
                        ,{ "UserName",cols[6]}
                    }
                );
            }

        }

        return list;
    }

    /// <summary>
    /// 获取日志列表
    /// </summary>
    /// <returns></returns>
    public static List<Dictionary<string, object>> GetLogFileList()
    {
        return LogFile.GetLogFileList();
    }


    #endregion


}