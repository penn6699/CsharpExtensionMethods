﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;


namespace ApiLogger
{

    /// <summary>
    /// 日志等级
    /// </summary>
    public static class LogLevel
    {
        /// <summary>
        /// 日志等级
        /// </summary>
        private static Dictionary<string, int> _Level = new Dictionary<string, int>() {
             { "error",0 }
            ,{ "trace", 1 }
            ,{ "debug", 2 }
        };

        /// <summary>
        /// 获取配置日志等级
        /// </summary>
        private static int ConfigLoggerLevel
        {
            get
            {
                string lel = ConfigurationManager.AppSettings["LoggerLevel"] ?? "Error";
                return _Level[lel.ToLower()];
            }
        }

        /// <summary>
        /// 是否可以写log
        /// </summary>
        /// <param name="Level"></param>
        /// <returns></returns>
        public static bool CanLog(string Level) {
            try
            {
                int lel= _Level[Level];
                return lel <= ConfigLoggerLevel;                
            }
            catch {
                return false;
            }
        }


    }




}