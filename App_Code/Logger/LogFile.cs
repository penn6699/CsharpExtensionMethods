﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;

namespace ApiLogger
{
    /// <summary>
    /// 日志文件读写
    /// </summary>
    public static class LogFile
    {

        #region 日志信息写到文件

        /// <summary>
        /// 日志信息队列
        /// </summary>
        private static Queue<string> _LogInfoQueue = new Queue<string>();

        /// <summary>
        /// 日志文件路径
        /// </summary>
        private static string CurrentLogFilePath
        {
            get
            {
                DateTime now = DateTime.Now;
                string path = string.Format(@"{0}log\{1}\{2}", HttpRuntime.AppDomainAppPath, now.ToString("yyyy"), now.ToString("MM"));
                string fileName = string.Format(@"\{0}.log", now.ToString("yyyyMMdd"));

                if (!Directory.Exists(path))
                {
                    Directory.CreateDirectory(path);
                }

                if (!File.Exists(path + fileName))
                {
                    FileStream fs = File.Create(path + fileName);
                    fs.Close();
                    fs.Dispose();
                }
                return path + fileName;
            }
        }

        /// <summary>
        /// 文件锁
        /// </summary>
        private static object locker = new object();

        /// <summary>
        /// 日志文件写入
        /// </summary>
        private static void WriterFileWork()
        {
            while (true)
            {
                try
                {
                    if (_LogInfoQueue.Count > 0)
                    {
                        lock (locker)
                        {
                            string logInfo = "";
                            try
                            {
                                logInfo = _LogInfoQueue.Dequeue();
                                StreamWriter writer = new StreamWriter(CurrentLogFilePath, true);

                                writer.WriteLine(logInfo);
                                writer.Flush();
                                writer.Close();

                            }
                            catch (Exception exp)
                            {
                                Write(LogFormatter.Format("error", "LogFile 写日志信息到文件失败。", exp, "Logger"));
                                if (logInfo != "")
                                {
                                    Write(logInfo + "");
                                }
                            }

                        }

                    }
                }
                catch { }

                Thread.Sleep(300);
            }
        }

        /// <summary>
        /// 是否初始化
        /// </summary>
        private static bool IsInit = false;

        #endregion


        #region 写日志到队列

        /// <summary>
        /// 写日志到队列
        /// </summary>
        /// <param name="logInfo"></param>
        public static void Write(string logInfo)
        {
            //线程初始化
            if (!IsInit)
            {
                Thread thread = new Thread(WriterFileWork);
                thread.Name = "ApiLoggerThread";
                thread.IsBackground = true;
                thread.Start();

                IsInit = true;
            }
            _LogInfoQueue.Enqueue(logInfo);

        }


        #endregion



        #region 读取日志文件

        /// <summary>
        /// 读取当前日志文件
        /// </summary>
        /// <returns></returns>
        private static string ReadCurrentLogFile()
        {
            lock (locker)
            {
                StreamReader sr = null;
                try
                {
                    sr = new StreamReader(CurrentLogFilePath);
                    return sr.ReadToEnd();
                }
                catch (Exception exp)
                {
                    throw new Exception("读取当前日志文件失败。" + exp.Message);
                }
                finally
                {
                    if (sr != null)
                    {
                        sr.Close();
                        sr.Dispose();
                    }
                }
            }
        }

        /// <summary>
        /// 读取当前日志文件
        /// </summary>
        /// <returns></returns>
        public static string ReadLogFile(string fileName = null)
        {

            StreamReader sr = null;
            try
            {
                DateTime now = DateTime.Now;

                string CurrentFileName = string.Format(@"\{0}.log", now.ToString("yyyyMMdd"));
                if (fileName == null || fileName.Equals(CurrentFileName))
                {
                    return ReadCurrentLogFile();
                }

                string logPath = string.Format(@"{0}log\{1}\{2}\{3}", HttpRuntime.AppDomainAppPath, fileName.Substring(0, 4), fileName.Substring(4, 2), fileName);

                List<Dictionary<string, object>> res = new List<Dictionary<string, object>>();
                //string logPath = HttpRuntime.AppDomainAppPath + path;

                sr = new StreamReader(logPath);
                return sr.ReadToEnd();
            }
            catch (Exception exp)
            {
                throw new Exception("读取信息日志文件失败。" + exp.Message);
            }
            finally
            {
                if (sr != null)
                {
                    sr.Close();
                    sr.Dispose();
                }
            }

        }

        /// <summary>
        /// 获取日志列表
        /// </summary>
        /// <returns></returns>
        public static List<Dictionary<string, object>> GetLogFileList()
        {

            List<Dictionary<string, object>> res = new List<Dictionary<string, object>>();
            string logPath = HttpRuntime.AppDomainAppPath + "log";

            if (!Directory.Exists(logPath))
            {
                return res;
            }

            DirectoryInfo root = new DirectoryInfo(logPath);
            foreach (DirectoryInfo year in root.GetDirectories())
            {
                foreach (DirectoryInfo month in year.GetDirectories())
                {
                    //2018-08
                    foreach (FileInfo logFile in month.GetFiles())
                    {
                        Dictionary<string, object> log = new Dictionary<string, object>();
                        log.Add("year", year.Name);
                        log.Add("month", month.Name);
                        log.Add("fileName", logFile.Name);
                        //log.Add("filePath", string.Format(@"log\{0}\{1}\{2}", year.Name, month.Name, logFile.Name));
                        log.Add("fileUpdateTime", logFile.LastWriteTime.ToString("yyyy-MM-dd HH:mm:ss"));
                        log.Add("fileSize", logFile.Length);//字节

                        res.Add(log);
                    }
                }
            }

            return res;

        }





        #endregion


        

    }
}