﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Web;


namespace ApiLogger
{
    /// <summary>
    /// 日志格式器
    /// </summary>
    public static class LogFormatter
    {
        /// <summary>
        /// 错误次数
        /// </summary>
        private static int ErrorCount = 0;

        /// <summary>
        /// 格式化
        /// </summary>
        /// <param name="LogLevel">级别</param>
        /// <param name="Message">消息</param>
        /// <param name="Data">数据</param>
        /// <param name="LogType">类型</param>
        /// <returns></returns>
        public static string Format(string LogLevel, string Message, object Data = null, string LogType = "系统") {
            try
            {
                if (ErrorCount > 1) {
                    ErrorCount = 0;
                }

                string _data = "";
                if (Data == null || Data is DBNull)
                {
                    _data = "";
                }
                
                else if (Data is Exception) {
                    Exception exp = (Exception) Data;
                    string expData = LogJson.Serialize(exp.Data);
                    _data = LogJson.Serialize(new Dictionary<string, string>() {
                        { "ExceptionMessage",exp.Message},
                        { "ExceptionData",expData},
                        { "ExceptionStackTrace",exp.StackTrace}
                    });
                }

                else if (
                    Data is sbyte || Data is short || Data is int ||Data is long || Data is byte || Data is ushort ||
                    Data is uint || Data is ulong || Data is decimal ||
                    Data is bool ||
                    Data is char || Data is string || Data is System.Text.StringBuilder
                )
                {
                    _data = Data.ToString();
                }
                else
                {
                    _data = LogJson.Serialize(Data);
                }

                

                string UserID = "";
                string UserName = "";

                if (SessionHelper.IsOnline()) {
                    LoginUserInfo user = (LoginUserInfo)SessionHelper.Current.Get("LoginUserInfo");
                    if (user != null) {
                        UserID = user.user_id.ToString();
                        UserName = user.user_realname;
                    }
                }

                if (LogType == "Logger") {
                    UserID = "logger";
                    UserName = "系统日志助手";
                }

                //时间 级别 类型 消息 数据 使用用户ID 使用用户Name
                string logInfo = string.Format("{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}",
                    DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss"),
                    LogLevel,
                    string.IsNullOrEmpty(LogType) ? "系统" : LogType.Replace("\t", @"&nbsp;").Replace("\r\n", @"<br/>").Replace("\n", @"<br/>"),
                    string.IsNullOrEmpty(Message) ? "" : Message.Replace("\t", @"&nbsp;").Replace("\r\n", @"<br/>").Replace("\n", @"<br/>"),
                    string.IsNullOrEmpty(_data) ? "" : _data.Replace("\t", @"&nbsp;").Replace("\r\n", @"<br/>").Replace("\n", @"<br/>"),
                    string.IsNullOrEmpty(UserID) ? "system" : UserID.Replace("\t", @"&nbsp;").Replace("\r\n", @"<br/>").Replace("\n", @"<br/>"),
                    string.IsNullOrEmpty(UserName) ? "系统" : UserName.Replace("\t", @"&nbsp;").Replace("\r\n", @"<br/>").Replace("\n", @"<br/>")
                );
                
                return logInfo;
            }
            catch (Exception exp) {  
                              
                ErrorCount += 1;

                //允许第一次格式化错误，并写日志
                if (ErrorCount > 1 )
                {
                    throw exp;
                }

                return Format("error", "日志信息格式化失败", exp, "Logger");
                
            }
        }
        
    }

}
