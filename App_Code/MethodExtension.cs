﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

/// <summary>
/// 扩展方法
/// </summary>
public static class ExtensionMethods
{

    #region Guid

    /// <summary>
    /// Guid 格式化
    /// </summary>
    /// <param name="guid">System.Guid 结构对象</param>
    /// <param name="mode">Guid 模式</param>
    /// <returns></returns>
    public static string Format(this Guid guid, GuidHelper.GuidMode mode)
    {
        //声明扩展方法
        //扩展方法必须是静态的，Add有三个参数
        //this 必须有，string表示我要扩展的类型，stringName表示对象名
        //三个参数this和扩展的类型必不可少，对象名可以自己随意取如果需要传递参数，
        //再增加一个变量即可
        return guid.ToString(Enum.GetName(typeof(GuidHelper.GuidMode), mode));
    }

    /// <summary>
    /// Guid 格式化
    /// </summary>
    /// <param name="guid">System.Guid 结构对象</param>
    /// <returns></returns>
    public static string Format(this Guid guid)
    {
        //声明扩展方法
        //扩展方法必须是静态的，Add有三个参数
        //this 必须有，string表示我要扩展的类型，stringName表示对象名
        //三个参数this和扩展的类型必不可少，对象名可以自己随意取如果需要传递参数，
        //再增加一个变量即可
        return guid.ToString();
    }

    #endregion

    #region DataTable

    /// <summary>
    /// 转换为字典集合
    /// </summary>
    /// <param name="dt"></param>
    /// <returns></returns>
    public static List<Dictionary<string, object>> ToDictionaryList(this DataTable dt) {
        List<Dictionary<string, object>> table = new List<Dictionary<string, object>>();
        foreach (DataRow dr in dt.Rows)
        {
            Dictionary<string, object> row = new Dictionary<string, object>();
            foreach (DataColumn dc in dt.Columns)
            {
                row[dc.ColumnName] = dr[dc.ColumnName];
            }
            table.Add(row);
        }
        return table;
    }


    #endregion


}
